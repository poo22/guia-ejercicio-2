﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApp1
{
    public class Surtidor
    {
        const double LITROS_MAXIMO = 50;

        private Nafta nafta = new Nafta();

        public Nafta Nafta
        {
            get { return nafta; }
            set { nafta = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


        private double cantidadRecaudado;

        public double CantidadRecaudado
        {
            get { return cantidadRecaudado; }
            set { cantidadRecaudado = value; }
        }

        private int cantidadClientes;

        public int CantidadClientes
        {
            get { return cantidadClientes; }
            set { cantidadClientes = value; }
        }

        private double litrosActual = LITROS_MAXIMO;

        public double LitrosActual
        {
            get { return litrosActual; }
            set { litrosActual = value; }
        }

        private double litrosMaximo = LITROS_MAXIMO;
        public double LitrosMaximo
        {
            get { return litrosMaximo; }
        }

        private bool estaVacio = false;

        public bool EstaVacio
        {
            get { return estaVacio; }
            set { estaVacio = value; }
        }

        private int cantidadRecargas = 0;

        public int CantidadRecargas
        {
            get { return cantidadRecargas; }
            set { cantidadRecargas = value; }
        }



        public bool Recargar()
        {
            if (litrosActual > 0) return false;
            litrosActual = LITROS_MAXIMO;
            estaVacio = false;
            cantidadRecargas++;
            return true; 
        }

    }
}