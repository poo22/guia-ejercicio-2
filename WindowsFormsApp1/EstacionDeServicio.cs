﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApp1
{
    public class EstacionDeServicio
    {
        private int canidadRecargas;

        public int CantidadRecargas
        {
            get { return canidadRecargas; }
            set { canidadRecargas = value; }
        }


        private int cantidadTotalClientes;

        public int CantidadTotalClientes
        {
            get { return cantidadTotalClientes; }
            set { cantidadTotalClientes = value; }
        }

        private double  cantidadRecaudado;

        public double CantidadRecaudado
        {
            get { return cantidadRecaudado; }
            set { cantidadRecaudado = value; }
        }

        private Surtidor normal;

        public Surtidor Normal
        {
            get { return normal; }
            set { normal = value; }
        }

        private Surtidor super;

        public Surtidor Super    
        {
            get { return super; }
            set { super = value; }
        }

        private Surtidor premium;

        public Surtidor Premium
        {
            get { return premium; }
            set { premium = value; }
        }

        public Venta venta;





        public bool IniciarVenta()
        {
            if (venta == null)
            {
                venta = new Venta();
                return true;
            }
            return false; 
        }
        public bool FinalizarVenta()
        {
            if (venta != null)
            {
                venta = null;
                return true;
            }
            return false;
        }

        public bool HayVentaActiva()
        {
            return (venta != null);
        }
        public void InicializarComponentes()
        {
            this.normal = new Surtidor();
            this.normal.Nafta.Tipo = "Normal";
            this.normal.Nafta.Precio = 17.20;
            this.normal.Nombre = "Surtidor Normal";

            this.super = new Surtidor();
            this.super.Nafta.Tipo = "Super";
            this.super.Nafta.Precio = 18.85;
            this.super.Nombre = "Surtidor Super";

            this.premium = new Surtidor();
            this.premium.Nafta.Tipo = "Premium";
            this.premium.Nafta.Precio = 21.30;
            this.premium.Nombre = "Surtidor Premium";
        }
        
         public Surtidor ObtenerSurtidorMayorRecaudacion()
         {
            double maximo = normal.CantidadRecaudado;
            Surtidor surtidorMayorRecaudacion = normal;

            if (maximo < super.CantidadRecaudado)
            {
                surtidorMayorRecaudacion = super;
                maximo = super.CantidadRecaudado;
            }
            if (maximo < premium.CantidadRecaudado)
            {
                surtidorMayorRecaudacion = premium;
                maximo = premium.CantidadRecaudado;
            }

            return surtidorMayorRecaudacion;
        }

        public Surtidor ObtenerSurtidorMenorRecaudacion()
        {
            double minimo = normal.CantidadRecaudado;
            Surtidor surtidorMenorRecaudacion = normal;

            if (minimo > super.CantidadRecaudado)
            {
                surtidorMenorRecaudacion = super;
                minimo = super.CantidadRecaudado;
            }
            if (minimo > premium.CantidadRecaudado)
            {
                surtidorMenorRecaudacion = premium;
                minimo = premium.CantidadRecaudado;
            }

            return surtidorMenorRecaudacion;
        }


        public Surtidor ObtenerSurtidorMayorCantCLientes()
        {
            double maximo = normal.CantidadClientes;
            Surtidor surtidorMayorCantClientes = normal;

            if (maximo < super.CantidadClientes)
            {
                surtidorMayorCantClientes = super;
                maximo = super.CantidadClientes;
            }
            if (maximo < premium.CantidadClientes)
            {
                surtidorMayorCantClientes = premium;
            }

            return surtidorMayorCantClientes;
        }
        
        public double CalcularPorcentajeVenta (Surtidor surtidor)
        {
            return surtidor.CantidadClientes * 100 / cantidadTotalClientes;
        }

        public double CalcularPorcentajeRecaudacion(Surtidor surtidor)
        {
            return Math.Round(surtidor.CantidadRecaudado * 100 / cantidadRecaudado, 2 );
        }


        public Surtidor ObtenerSurtidorMayorCatRecargas()
        {
            double maximo = normal.CantidadRecargas;
            Surtidor surtidorMasRecargas = normal;

            if (maximo < super.CantidadRecargas)
            {
                surtidorMasRecargas = super;
                maximo = super.CantidadRecargas;
            }
            if (maximo < premium.CantidadRecargas)
            {
                surtidorMasRecargas = premium;
            }

            if (surtidorMasRecargas.CantidadRecargas == 0) return null;
            return surtidorMasRecargas;
        }


        public double CalcularPromediodeVenta()
        {
            return Math.Round((double)CantidadTotalClientes/3, 2);
        }

        public double CalcularPromediodeRecaudacion()
        {
            return Math.Round(cantidadRecaudado / 3,2);
        }


    }
}