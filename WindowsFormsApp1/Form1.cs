﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public EstacionDeServicio estacionDeServicio = new EstacionDeServicio();


        public Form1()
        {
            InitializeComponent();

            estacionDeServicio.InicializarComponentes();


            listBox1.Items.Add(estacionDeServicio.Normal);
            listBox1.Items.Add(estacionDeServicio.Super);
            listBox1.Items.Add(estacionDeServicio.Premium);
            listBox1.DisplayMember = "Nombre";

            MostrarLitrosRestantes();

        }



        private void button3_Click(object sender, EventArgs e)
        {
            estacionDeServicio.IniciarVenta();
            if (!estacionDeServicio.HayVentaActiva())
            {
                MessageBox.Show("No hay una venta comenzada");
                return; 
            }
            if (string.IsNullOrEmpty(textBox1.Text) )
            {
                MessageBox.Show("Elija cuantos litros desea cargar");
                return;
            }

            if (listBox1.SelectedItem == null)
            {
                MessageBox.Show("Seleccione un surtidor");
                return;
            }

            estacionDeServicio.venta.Agregar((Surtidor)listBox1.SelectedItem, double.Parse(textBox1.Text)) ;
            
            if( ((Surtidor)listBox1.SelectedItem).EstaVacio ) 
            {

                MessageBox.Show($"Solo se pudo cargar {estacionDeServicio.venta.CantidadVendida}" +
                                $" litros. El surtidor quedo vacio.");
                this.MostrarLitrosRestantes();
                return;
            }
            
            estacionDeServicio.CantidadRecaudado += estacionDeServicio.venta.ValorVendido;
            

            //Recaudaciones maximas
            label3.Text = "$" + estacionDeServicio.CantidadRecaudado.ToString();
            label4.Text = "$" + ((Surtidor)listBox1.SelectedItem).CantidadRecaudado.ToString();

            //surtidores Max y Min
            label6.Text = estacionDeServicio.ObtenerSurtidorMayorRecaudacion().Nombre.ToString();
            label8.Text = estacionDeServicio.ObtenerSurtidorMenorRecaudacion().Nombre.ToString();


            //CLientes
            ((Surtidor)(listBox1.SelectedItem)).CantidadClientes++;
            estacionDeServicio.CantidadTotalClientes++;
            label10.Text = estacionDeServicio.ObtenerSurtidorMayorCantCLientes().Nombre.ToString();

            //Porcentaje Ventas
            label12.Text = estacionDeServicio.Normal.Nombre + ": " +
                           estacionDeServicio.CalcularPorcentajeVenta(estacionDeServicio.Normal) + "%";

            label15.Text = estacionDeServicio.Super.Nombre + ": " +
                           estacionDeServicio.CalcularPorcentajeVenta(estacionDeServicio.Super) + "%";

            label14.Text = estacionDeServicio.Premium.Nombre + ": " +
                           estacionDeServicio.CalcularPorcentajeVenta(estacionDeServicio.Premium) + "%";


            //Porcentaje Recaudaciones
            label18.Text = estacionDeServicio.Normal.Nombre + ": " +
                           estacionDeServicio.CalcularPorcentajeRecaudacion(estacionDeServicio.Normal) + "%";

            label16.Text = estacionDeServicio.Super.Nombre + ": " +
                           estacionDeServicio.CalcularPorcentajeRecaudacion(estacionDeServicio.Super) + "%";

            label17.Text = estacionDeServicio.Premium.Nombre + ": " +
                           estacionDeServicio.CalcularPorcentajeRecaudacion(estacionDeServicio.Premium) + "%";


            this.MostrarLitrosRestantes();
            this.MostrarSurtidorMasRecargas();


            //Promedio ventasy recaudacion
            label28.Text = estacionDeServicio.CalcularPromediodeRecaudacion().ToString();
            label26.Text = estacionDeServicio.CalcularPromediodeVenta().ToString();



            estacionDeServicio.FinalizarVenta();

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                label4.Text = "$" + ((Surtidor)listBox1.SelectedItem).CantidadRecaudado.ToString();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
            {
                MessageBox.Show("Seleccione un surtidor");
                return;
            }
            if (!((Surtidor)listBox1.SelectedItem).Recargar())
            {
                MessageBox.Show("El surtidor todavia no esta vacio");
                return;
            }
            MostrarLitrosRestantes();
            MostrarSurtidorMasRecargas();
        }

        public void MostrarLitrosRestantes()
        {
            label22.Text = estacionDeServicio.Normal.Nombre + ": " +
                           estacionDeServicio.Normal.LitrosActual;

            label20.Text = estacionDeServicio.Super.Nombre + ": " +
                           estacionDeServicio.Super.LitrosActual;

            label21.Text = estacionDeServicio.Premium.Nombre + ": " +
                           estacionDeServicio.Premium.LitrosActual;
        }

        public void MostrarSurtidorMasRecargas()
        {
            label24.Text = (estacionDeServicio.ObtenerSurtidorMayorCatRecargas() == null)? "-":estacionDeServicio.ObtenerSurtidorMayorCatRecargas().Nombre.ToString() ;
        }


    }
}
