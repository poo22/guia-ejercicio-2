﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApp1
{
    public class Venta
    {

        private double valorVendido;

        public double ValorVendido
        {
            get { return valorVendido; }
            set { valorVendido = value; }
        }


        private double cantidadVendida;

        public double CantidadVendida
        {
            get { return cantidadVendida; }
            set { cantidadVendida = value; }
        }


        public void Agregar(Surtidor surtidor, double litrosAVender )
        {
            this.cantidadVendida = litrosAVender;
            double nuevosLitrosActual = surtidor.LitrosActual - this.cantidadVendida;
            if (nuevosLitrosActual < 0)
            {
                cantidadVendida = surtidor.LitrosActual;
                nuevosLitrosActual = 0;
                surtidor.EstaVacio = true;
            }

            this.valorVendido= surtidor.Nafta.Precio * this.cantidadVendida;
            surtidor.CantidadRecaudado += this.valorVendido;
            surtidor.LitrosActual = nuevosLitrosActual;

        }


    }
}